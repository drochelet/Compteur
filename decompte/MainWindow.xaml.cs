﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace decompte
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.compteur.Content = jours();

        }

        private String jours()
        {
            DateTime cible = new DateTime(2018,07,29,08,00,00); // dimanche à 8h
            DateTime jours = DateTime.Now;
            String reste = String.Concat((cible - jours).ToString(), " jours");

            return reste;
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            this.compteur.Content = jours();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
        // do not use
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            this.compteur.Content = jours();
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
